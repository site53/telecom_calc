$(function(){
	$('.switch_container').on('click', function(){
		$(this).closest('.switch').toggleClass('active');
	});
    

    $('.calc_button').on('click', function(){
    	$('.calc_form_block_wr').slideDown();
    	$('html, body').animate({
	      scrollTop: $('.calc_form_block_wr').offset().top - 0
	  }, 1200)
    });

    $('.calc_edit_btn').on('click', function(){
    	$('html, body').animate({
	      scrollTop: $('.calc_block').offset().top - 0
	  }, 1200)
    });
});

$(function() {
	const min = 30;
	const min_medium = 50;
	const medium = 100;
	const max_medium = 150;
	const max = 200;
	$('.calc_left').on('change click', function() {
		///let value = $('input:radio:checked').val();
		let value = 30;
		$('.calc_left .radio input:radio:checked').each(function() {
			if ( $(this).val() == 'min_medium' && value < min_medium ) {
				value = min_medium;
			} else if ( $(this).val() == 'medium' && value < medium ) {
				value = medium;
			} else if ( $(this).val() == 'max_medium' && value < max_medium ) {
				value = max_medium;
			} else if ( $(this).val() == 'max' && value < max ) {
				value = max;
			} else if ( $(this).val() == 'min' && value == min_medium ) {
				value = min;
				return false;
			}
		});

		let optionLength = $(this).find('.switch.active').length;

		if (optionLength == 2 && value < max_medium) {
			value = max_medium;
		} else if (optionLength > 2) {
			value = max;
		}

		$('.calc_right .rezult_main, .right_form .rezult_main').text(value);
		answerRendered();
	});

	function answerRendered() {
		let answerRender = [];
		$('.calc_left .switch').each(function() {
			let text = $(this).find('.switch_text').text();
			if ($(this).hasClass('active')) {
				answerRender.push(text + ' - ' + 'Да')
			}
		})
		$('.calc_left input:radio:checked').each(function () {
			let valChecked = $(this).parent().text().trim();
			let nameChecked = $(this).closest('.field').find('.field_name').text();
			answerRender.push(nameChecked + ' - ' + valChecked);
			$('.calc_form_block .hidden-field textarea').val(answerRender)
		});

	}

	answerRendered();
});

